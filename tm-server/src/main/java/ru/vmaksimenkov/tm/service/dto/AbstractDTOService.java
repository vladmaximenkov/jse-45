package ru.vmaksimenkov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import ru.vmaksimenkov.tm.api.service.IConnectionService;
import ru.vmaksimenkov.tm.api.service.dto.IAbstractDTOService;
import ru.vmaksimenkov.tm.dto.AbstractBusinessEntityDTO;

public abstract class AbstractDTOService<E extends AbstractBusinessEntityDTO> implements IAbstractDTOService<E> {

    @NotNull
    private IConnectionService connectionService;

    public AbstractDTOService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

}
