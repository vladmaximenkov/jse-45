package ru.vmaksimenkov.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import ru.vmaksimenkov.tm.exception.entity.RoleNotFoundException;

import static ru.vmaksimenkov.tm.util.ValidationUtil.checkRole;

@Getter
public enum Role {

    USER("UserDTO"),
    ADMIN("Administrator");

    @NotNull
    private final String displayName;

    Role(@NotNull final String displayName) {
        this.displayName = displayName;
    }

    public static @NotNull Role getRole(String s) {
        s = s.toUpperCase();
        if (!checkRole(s)) throw new RoleNotFoundException();
        return valueOf(s);
    }

}
