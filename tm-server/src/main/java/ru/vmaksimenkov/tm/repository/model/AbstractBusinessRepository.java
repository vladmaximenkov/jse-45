package ru.vmaksimenkov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.api.repository.model.IAbstractBusinessRepository;
import ru.vmaksimenkov.tm.model.AbstractBusinessEntity;

import javax.persistence.EntityManager;
import java.util.Objects;

public abstract class AbstractBusinessRepository<E extends AbstractBusinessEntity> extends AbstractRepository<E> implements IAbstractBusinessRepository<E> {

    public AbstractBusinessRepository(@NotNull final EntityManager em) {
        super(em);
    }

    @Nullable
    public E findByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (index == null) return null;
        return Objects.requireNonNull(findAll(userId)).get(index);
    }

    @Nullable
    @Override
    public String getIdByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (index == null) return null;
        return Objects.requireNonNull(findAll(userId)).get(index).getId();
    }

    @Override
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        remove(findById(userId, id));
    }

    @Override
    public void removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        remove(findByIndex(userId, index));
    }
}