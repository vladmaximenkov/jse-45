package ru.vmaksimenkov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.api.repository.model.IAbstractRepository;
import ru.vmaksimenkov.tm.model.AbstractEntity;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.Collection;
import java.util.List;

public abstract class AbstractRepository<E extends AbstractEntity> implements IAbstractRepository<E> {

    @NotNull
    protected final EntityManager em;

    public AbstractRepository(@NotNull final EntityManager em) {
        this.em = em;
    }

    public void add(@Nullable final E entity) {
        em.persist(entity);
    }

    public void add(@Nullable final List<E> entities) {
        if (entities == null) return;
        for (E item : entities) {
            add(item);
        }
    }

    @NotNull
    public abstract List<E> findAll();

    @Nullable
    public abstract E findById(@Nullable final String id);

    @Nullable
    public E findByIndex(final int index) {
        return findAll().get(index);
    }

    @Nullable
    public E getEntity(@NotNull final TypedQuery<E> query) {
        @NotNull final List<E> resultList = query.getResultList();
        if (resultList.isEmpty()) return null;
        return resultList.get(0);
    }

    public boolean isEmpty() {
        return size() == 0;
    }

    public void remove(@Nullable final E entity) {
        if (entity == null) return;
        em.remove(entity);
    }

    public void remove(@Nullable final Collection<E> entities) {
        if (entities == null || entities.isEmpty()) return;
        for (@Nullable final E entity : entities) remove(entity);
    }

    public void removeById(@Nullable final String id) {
        remove(findById(id));
    }

    public void removeByIndex(final int index) {
        E entity = findByIndex(index);
        if (entity == null) return;
        this.removeById(entity.getId());
    }

    public abstract Long size();

    public void update(@Nullable final E entity) {
        em.merge(entity);
    }

}