package ru.vmaksimenkov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.vmaksimenkov.tm.api.other.ISaltSettings;

public interface IPropertyService extends ISaltSettings {

    @NotNull String getApplicationVersion();

    int getBackupInterval();

    @NotNull String getCacheProvider();

    @NotNull String getFactoryClass();

    @NotNull String getJdbcDialect();

    @NotNull String getJdbcDriver();

    @NotNull String getJdbcHBM2DDL();

    @NotNull String getJdbcPassword();

    @NotNull String getJdbcShowSql();

    @NotNull String getJdbcUrl();

    @NotNull String getJdbcUsername();

    @NotNull String getLiteMember();

    @NotNull String getMinimalPuts();

    @NotNull String getQueryCache();

    @NotNull String getRegionPrefix();

    int getScannerInterval();

    @NotNull String getSecondLevelCash();

    @NotNull String getServerHost();

    int getServerPort();

    int getSessionCycle();

    @NotNull String getSessionSalt();

}
