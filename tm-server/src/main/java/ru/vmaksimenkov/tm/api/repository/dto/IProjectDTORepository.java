package ru.vmaksimenkov.tm.api.repository.dto;

import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.dto.ProjectDTO;

public interface IProjectDTORepository extends IAbstractBusinessDTORepository<ProjectDTO> {

    boolean existsByName(@Nullable String userId, @Nullable String name);

    @Nullable
    ProjectDTO findByName(@Nullable String userId, @Nullable String name);

    @Nullable
    String getIdByName(@Nullable String userId, @Nullable String name);

    void removeByName(@Nullable String userId, @Nullable String name);

}
