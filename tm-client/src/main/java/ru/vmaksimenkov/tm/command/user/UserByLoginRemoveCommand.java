package ru.vmaksimenkov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.endpoint.Role;

import static ru.vmaksimenkov.tm.util.TerminalUtil.nextLine;

public final class UserByLoginRemoveCommand extends AbstractUserCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Remove user by login";
    }

    @NotNull
    @Override
    public String commandName() {
        return "user-remove-by-login";
    }

    @NotNull
    @Override
    public Role[] commandRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE USER]");
        System.out.println("ENTER LOGIN:");
        endpointLocator.getAdminUserEndpoint().removeUserByLogin(endpointLocator.getSession(), nextLine());
    }

}
