package ru.vmaksimenkov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.endpoint.Role;
import ru.vmaksimenkov.tm.endpoint.SessionDTO;
import ru.vmaksimenkov.tm.exception.user.NotLoggedInException;

public final class UserLogoutCommand extends AbstractUserCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Log out of the system";
    }

    @NotNull
    @Override
    public String commandName() {
        return "user-logout";
    }

    @NotNull
    @Override
    public Role[] commandRoles() {
        return Role.values();
    }

    @Override
    public void execute() {
        @Nullable final SessionDTO session = endpointLocator.getSession();
        if (session == null) throw new NotLoggedInException();
        endpointLocator.getSessionEndpoint().closeSession(session);
        endpointLocator.setSession(null);
    }

}
