package ru.vmaksimenkov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.api.IRepository;
import ru.vmaksimenkov.tm.endpoint.TaskDTO;

import java.util.List;

public interface ITaskRepository extends IRepository<TaskDTO> {

    void bindTaskPyProjectId(@NotNull String userId, @NotNull String projectId, @NotNull String taskId);

    boolean existsByName(@NotNull String userId, @NotNull String name);

    boolean existsByProjectId(@NotNull String userId, @NotNull String projectId);

    @Nullable
    List<TaskDTO> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

    @Nullable
    TaskDTO findOneByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    TaskDTO findOneByName(@NotNull String userId, @NotNull String name);

    @Nullable
    String getIdByIndex(@NotNull String userId, @NotNull Integer index);

    void removeAllBinded(@NotNull String userId);

    void removeAllByProjectId(@NotNull String userId, @NotNull String projectId);

    void removeOneByIndex(@NotNull String userId, @NotNull Integer index);

    void removeOneByName(@NotNull String userId, @NotNull String name);

    void unbindTaskFromProject(@NotNull String userId, @NotNull String taskId);

}
